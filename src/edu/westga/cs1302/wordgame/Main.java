package edu.westga.cs1302.wordgame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Main extends the JavaFX application class to build the GUI
 * and start program execution.
 * 
 * @author Kameron Martin
 * @version Spring 2018
 */
public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("view/GameGui.fxml"));
			Pane pane = loader.load();
			Scene scene = new Scene(pane);
			primaryStage.setTitle("A Word Game by Kameron Martin");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Launches the application.
	 * 
	 * @param args
	 * 			  not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
