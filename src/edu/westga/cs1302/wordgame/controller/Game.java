package edu.westga.cs1302.wordgame.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * The Controller Class.
 * 
 * @author CS 1302
 * @version Spring 2018
 */
public class Game {
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    

    @FXML
    private Button newGame;

    @FXML
    private Label myScore;

    @FXML
    private Button shuffleWords;

    @FXML
    private TextField userInput;

    @FXML
    private Button submitButton;

    @FXML
    private TextArea submittedWords;
    
    @FXML
    private Label wordForGame;
    
    private ArrayList<String> wordList;
    private Random rand;
    private int gameScore;
    
    /**
     * Game constructor for game
     * 
     * @precondition none
     * @postcondition none
     * 
     * @throws FileNotFoundException
     * 			throws FileNotFoundException if file is not found.
     */
    public Game() throws FileNotFoundException {
    	this.wordList = new ArrayList<String>();
    	this.rand = new Random();
    	
    	File file = new File("dictionary.txt");
    	Scanner input = new Scanner(file);
    	
    	while (input.hasNextLine()) {
    		try {
    			String word = input.nextLine();
    			this.wordList.add(word);
    		} catch (Exception e) {
    			System.err.println(e.getMessage());
    		}
    	
    	
    	}
    	input.close();
    }

    @FXML
    void initialize() {
    	  assert this.newGame != null : "fx:id=\"newGame\" was not injected: check your FXML file 'GameGui.fxml'.";
          assert this.myScore != null : "fx:id=\"myScore\" was not injected: check your FXML file 'GameGui.fxml'.";
          assert this.wordForGame != null : "fx:id=\"wordForGame\" was not injected: check your FXML file 'GameGui.fxml'.";
          assert this.shuffleWords != null : "fx:id=\"shuffleWords\" was not injected: check your FXML file 'GameGui.fxml'.";
          assert this.userInput != null : "fx:id=\"userInput\" was not injected: check your FXML file 'GameGui.fxml'.";
          assert this.submitButton != null : "fx:id=\"submitButton\" was not injected: check your FXML file 'GameGui.fxml'.";
          assert this.submittedWords != null : "fx:id=\"submittedWords\" was not injected: check your FXML file 'GameGui.fxml'.";
          
        this.wordForGame.setText("");
        this.myScore.setText("Score: 0");
        this.userInput.setText("");
        this.submittedWords.setText("");
    }
    
    @FXML
    void handleNewGame() {
    	this.setNewGame();
    }
    
    @FXML
    void enterWord() {
    	this.enteringAWord();
    }
    
    @FXML
    void handleShuffleWord() {
    	
    	if (this.wordForGame.getText().isEmpty()) {
    		return;
    	}
    	
    	String game = this.wordForGame.getText().toLowerCase().replace(" ", "");
    	game = this.shuffleWord(game);
    	game = game.toUpperCase().replace("", " ").trim();
    	this.wordForGame.setText(game);
    }
    
    private String shuffleWord(String word) {
    	String shuffledWord = word; 
    	int wordSize = word.length();
    	int shuffleCount = 12;
    	
    	for (int i = 0; i < shuffleCount; i++) {
    	    int position1 = ThreadLocalRandom.current().nextInt(0, wordSize);
    	    int position2 = ThreadLocalRandom.current().nextInt(0, wordSize);
    	    shuffledWord = this.swapCharacters(shuffledWord, position1, position2);
    	}
    	return shuffledWord;	
    }
    
    private String swapCharacters(String shuffledWord, int position1, int position2) {
    	char[] charArray = shuffledWord.toCharArray();
    	char temp = charArray[position1];
    	charArray[position1] = charArray[position2];
    	charArray[position2] = temp;
    	return new String(charArray);
    }
    
    
    private void setNewGame() {
    	this.gameScore = 0;
        this.myScore.setText("Score: " + this.gameScore);
        this.userInput.setText("");
        this.submittedWords.setText("");
    	
    	int size = this.wordList.size();
    	
    	
    	String word = "";
    	
    	
    	
    	while (word.length() < 6) {
    		int randomWord = this.rand.nextInt(size);
    		word = this.wordList.get(randomWord);
    	}
    	
    	word = this.shuffleWord(word);
    	
    	word = word.toUpperCase().replace("", " ").trim();
    
    	
    	this.wordForGame.setText(word);
    }
    
    private void enteringAWord() {
    	String userWord = this.userInput.getText().toLowerCase();
    	String game = this.wordForGame.getText().toLowerCase().replace(" ", "");
    	
    	int count = 0;
    	for (int i = 0; i < userWord.length(); i++) {
    		char word = userWord.charAt(i);
    		
    		
    		if (game.contains(Character.toString(word))) {
    			count++;
    		}	
    	}
    	
    	if (this.wordList.contains(userWord)) {
    		if (count == userWord.length()) {
        		this.increaseScore();
        	}
    	}
    	
    	
    	
    }
    private void increaseScore() {
    	
    	
    	
    	String output = this.submittedWords.getText();
    	
    	if (output.contains(this.userInput.getText().toLowerCase())) {
    		return;
    	}
    	
    	if (output.isEmpty()) {
    		this.submittedWords.setText(this.userInput.getText().toLowerCase());
    	} else {
    		this.submittedWords.setText(output + "\n" + this.userInput.getText().toLowerCase());
    	}
    	
    	this.gameScore += this.userInput.getText().length();
    	this.myScore.setText("Score: " + this.gameScore);
    }
}


